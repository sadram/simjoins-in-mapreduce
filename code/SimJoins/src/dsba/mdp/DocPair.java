package dsba.mdp;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.TreeSet;

/**
 * Created by @yaj
 *
 * @date: 06/06/16.
 */
class DocPair implements WritableComparable<DocPair> {

    private Text mDoc1;
    private Text mDoc2;
    private TreeSet<Text> mDocTS;

    DocPair() {
        set(new Text(), new Text());
    }

    public DocPair(Text d1, Text d2) {
        set(d1, d2);
    }

    protected void set(Text first, Text second) {
        this.mDoc1 = first;
        this.mDoc2 = second;
        this.mDocTS = new TreeSet<>();
        this.mDocTS.add(first);
        this.mDocTS.add(second);
    }

    Text getDoc1() {
        return mDoc1;
    }

    Text getDoc2() {
        return mDoc2;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.mDoc1.readFields(in);
        this.mDoc2.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        this.mDoc1.write(out);
        this.mDoc2.write(out);
    }

    @Override
    public String toString() {
        return this.mDoc1 + ", " + this.mDoc2;
    }

    @Override
    public int compareTo(DocPair other) {
        int firstDocCompare = other.mDocTS.pollFirst().compareTo(this.mDocTS.pollFirst());
        if (firstDocCompare == 1) {
            return (other.mDocTS.pollLast().compareTo(this.mDocTS.pollLast()) >= 1 ? 1 : -1);
        } else {
            return (firstDocCompare > 1 ? 1 : -1);
        }
    }

    @Override
    public int hashCode() {
        return mDoc1.hashCode() * 29 + mDoc2.hashCode() * 37;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DocPair) {
            DocPair tmp = (DocPair) o;
            return mDoc1.equals(tmp.mDoc1) && mDoc2.equals(tmp.mDoc2);
        }
        return false;
    }
}