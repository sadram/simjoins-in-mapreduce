package dsba.mdp;

import java.util.TreeSet;

/**
 * Created by @yaj
 *
 * @date: 06/06/16.
 */
public class JaccardSimilarity {

    public double compute(TreeSet<String> ens1, TreeSet<String> ens2) {

        TreeSet<String> smallerSet = ens1;
        TreeSet<String> biggerSet = ens2;

        if (smallerSet.size() > biggerSet.size()) {
            smallerSet = ens1;
            biggerSet = ens2;
        }

        smallerSet.retainAll(biggerSet);
        biggerSet.addAll(smallerSet);
        // | ens1 n ens2 | / | ens1 U ens2 |
        return (double) smallerSet.size() /  biggerSet.size();
    }
}
