package dsba.mdp.assign2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by @yaj
 *
 * @date: 28/05/16.
 */
public class PreProcessInput extends Configured implements Tool {

    private static final Logger LOG = Logger.getLogger(PreProcessInput.class);
    public String MRtxtOutputSeparator = ";";
    public enum CUSTOM_COUNTER {NUM_LINES}

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new PreProcessInput(), args);
        System.exit(res);
    }

    public static void usage() {
        System.err.println("hadoop InputPreProcess.jar <inputPath> <outputPath> -stopwords <CSV file>");
        System.exit(-1);
    }

    public int run(String[] args) throws Exception {

        if (args.length != 4) {
            usage();
        }

        Job job = Job.getInstance(getConf(), "Input pre-processing");

        /*
        * Parse chuncked stopwords input into distributed cache
        * */
        for (int i = 0; i < args.length; i += 1) {
            if ("-stopwords".equals(args[i])) {
                job.getConfiguration().setBoolean("PreProcessInput.mStopWordsHS", true);
                job.addCacheFile(new Path(args[i++]).toUri());
                LOG.info("File " + args[i] + "added to distributed cache");
            }
        }
        /*
        * Job Setup / Args parsing
        * */
        job.setJarByClass(this.getClass());

        FileSystem fs = FileSystem.newInstance(getConf());
        // String inputPath = args[0] + "/" + args[1];
        String inputPath = args[0];
        if(!fs.exists(new Path(inputPath))) {
            System.err.println(inputPath + " does not exists");
            System.exit(-1);
        }

        // String outputPath = args[0] + "/" + args[2];
        String outputPath = args[1];
        if (fs.exists(new Path(args[1]))) {
            fs.delete(new Path(args[1]), true);
        }

        FileInputFormat.addInputPath(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.getConfiguration().set("mapreduce.output.textoutputformat.separator", MRtxtOutputSeparator);

        /*
        * Run the job and save the output to HDFS
        * */
        int res=job.waitForCompletion(true)?0:1;
        long recordCounter = job.getCounters().findCounter(CUSTOM_COUNTER.NUM_LINES).getValue();
        Path recordCounterFile = new Path("PreProcessInput" + "/" + "number_of_output_records.txt");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fs.create(recordCounterFile, true)));
        bw.write(String.valueOf(recordCounter));
        bw.close();
        return res;
    }

    private static class Map extends
            Mapper<LongWritable, Text, LongWritable, Text> {
        private Set<String> mStopWordsHS = new HashSet<>();
        private BufferedReader stopWordReader;

        /*
        * fetch stop words from cache
        * */
        protected void setup(Mapper.Context context) throws IOException,
                InterruptedException {
            if (context.getInputSplit() instanceof FileSplit) {
                ((FileSplit) context.getInputSplit()).getPath().toString();
            } else {
                context.getInputSplit().toString();
            }
            Configuration config = context.getConfiguration();
            if (config.getBoolean("PreProcessInput.mStopWordsHS", false)) {
                URI[] localPaths = context.getCacheFiles();
                getStopWords(localPaths[0]);
            }
        }
        /*
        * Generate stop word Hashset
        * */
        private void getStopWords(URI stopWordsURI) {
            LOG.info("File " + stopWordsURI + "added to distributed cache");
            try {
                stopWordReader = new BufferedReader(new FileReader(new File(stopWordsURI.getPath()).getName()));

                String a_stop_word;
                while ((a_stop_word = stopWordReader.readLine()) != null) {
                    mStopWordsHS.add(a_stop_word);
                }
            } catch (IOException Ex) {
                System.err.println("Error while parsing file in cache " + stopWordsURI);
                Ex.printStackTrace();
            }
        }


        // Compile regex patterns outside of the map to increase performance
        private final Pattern NON_ALNUM = Pattern.compile("[\\P{IsAlphabetic}\\P{IsDigit}]+");
        private final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

        // emits the couple (document index, words of document)
        public void map(LongWritable docIndex, Text line, Context context)
                throws IOException, InterruptedException {

            // skip empty lines
            if (!line.toString().isEmpty()) {
                // Create a list of words only containing alphanumeric
                String[] words = WORD_BOUNDARY.split(
                        NON_ALNUM.matcher(
                                line.toString().toLowerCase()).replaceAll(" "));

                // emit the line number (document index) and the word
                // only if it is not a stop word or an empty line.
                for (String word : words) {
                    if (!word.isEmpty() && !mStopWordsHS.contains(word)) {
                        context.write(docIndex, new Text(word));
                    }
                }
            }
        }
    }


    private static class Reduce extends
            Reducer<LongWritable, Text, LongWritable, Text> {


        @Override
        public void reduce(LongWritable docIndex, Iterable<Text> words, Context context)
                throws IOException, InterruptedException
        {
            // Create a list of words in the current document (remember a document is a line of the input file)
            ArrayList<String> wordCountPerIdx = new ArrayList<>();
            for (Text wrd: words) {
                wordCountPerIdx.add(wrd.toString());
            }
            // Generate a list of Pairs(word, count) of
            // each unique word per document and its count accross all documents
            // Pairs are sorted in ascending oreder by the TreeSet
            // with the override of the CompreTo method in Class Pair.
            HashSet<String> wcIdxHS = new HashSet<>(wordCountPerIdx);
            TreeSet<Pair> wcFreqHS = new TreeSet<>();
            for (String wrd: wcIdxHS) {
                if(wrd != null) {
                    wcFreqHS.add(new Pair(wrd, Collections.frequency(wordCountPerIdx, wrd)));
                }
            }

            // build a string made by an ascending order of word1#frequency1, ...
            StringBuilder sortedWcFreqSB = new StringBuilder();
            Iterator<Pair> PairItr = wcFreqHS.iterator() ;
            if (PairItr.hasNext()) {
                Pair firstPair = PairItr.next();
                sortedWcFreqSB.append(firstPair.word).append("#").append(firstPair.globalFrequency);
                while(PairItr.hasNext()) {
                    Pair nextPair = PairItr.next();
                    sortedWcFreqSB.append(", ").append(nextPair.word).append("#").append(nextPair.globalFrequency);
                }
            }

            // emit the global count of lines and the (key; value) = (line number; "word1#freq1,word2#freq2, ..."
            context.getCounter(CUSTOM_COUNTER.NUM_LINES).increment(1);
            context.write(docIndex, new Text(sortedWcFreqSB.toString()));

        }

        class Pair implements Comparable<Pair> {
            /**
             * A comparable class of pair composed by
             * a word a its global frequency acros the corpus document
             *
             */

            String word;
            int globalFrequency;

            Pair(String word, int globalFrequency) {
                this.globalFrequency = globalFrequency;
                this.word = word;
            }

            @Override
            public int compareTo(Pair pair) {
                return (this.globalFrequency >= pair.globalFrequency ? 1 : -1);
            }
        }
    }
}
