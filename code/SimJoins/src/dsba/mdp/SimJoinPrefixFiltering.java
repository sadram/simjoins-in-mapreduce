package dsba.mdp;

import java.io.*;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * Created by @yaj
 *
 * @date: 06/06/16.
 */
public class SimJoinPrefixFiltering extends Configured implements Tool {

    private static final Logger LOG = Logger.getLogger(SimJoinPrefixFiltering.class);
    public enum EnumCounter {PREFIX_FILTERING_CMP}

    private static void usage() {
        System.err.println("Usage: SimJoinPrefixFiltering <in> <out>");
        ToolRunner.printGenericCommandUsage(System.err);
        System.exit(2);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(Arrays.toString(args));
        int res = ToolRunner.run(new Configuration(),new SimJoinPrefixFiltering(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        System.out.println(Arrays.toString(args));
        Job simJoinPFJob = Job.getInstance(getConf(), "SimJoinPrefixFiltering");

        if (args.length !=2) {
            LOG.error(Arrays.toString(args));
            usage();
        }

        simJoinPFJob.setJarByClass(SimJoinPrefixFiltering.class);
        simJoinPFJob.setMapperClass(SimJoinPrefFilterMapper.class);
        simJoinPFJob.setReducerClass(SimJoinPrefFilterReducer.class);
        simJoinPFJob.setMapOutputKeyClass(Text.class);
        simJoinPFJob.setMapOutputValueClass(Text.class);
        simJoinPFJob.setOutputKeyClass(Text.class);
        simJoinPFJob.setOutputValueClass(Text.class);
        simJoinPFJob.setInputFormatClass(KeyValueTextInputFormat.class);
        simJoinPFJob.setOutputFormatClass(TextOutputFormat.class);
        simJoinPFJob.getConfiguration().set("mapreduce.input.keyvaluelinerecordreader.key.value.separator",",");
        simJoinPFJob.getConfiguration().set("mapreduce.output.textoutputformat.separator", ", ");
        simJoinPFJob.setNumReduceTasks(1);

        FileSystem fs = FileSystem.newInstance(getConf());


        Path inFile = new Path(args[0]);
        Path outFile = new Path("SimJoinPrefixFiltering" + "/" + args[1]);

        if (fs.exists(outFile)) {
            fs.delete(outFile, true);
            LOG.info("Deleted " + outFile);
        }

        FileInputFormat.addInputPath(simJoinPFJob, inFile);
        FileOutputFormat.setOutputPath(simJoinPFJob, outFile);

        int ret = (simJoinPFJob.waitForCompletion(true)?0:1);
        createCounterFile(simJoinPFJob, fs, new Path("SimJoinPrefixFiltering/PAIR_WISE_COMP.txt"));
        return ret;
    }

    private void createCounterFile(Job job, FileSystem fs, Path filename) throws IOException {
        if (fs.exists(filename)) {
            fs.delete(filename, true);
            LOG.info("Deleted " + filename);
        }
        long compCount = job.getCounters().findCounter(EnumCounter.PREFIX_FILTERING_CMP).getValue();
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(fs.create(filename, true)));
        br.write(String.valueOf(compCount));
        br.close();
    }

    private static BufferedReader openFileBR(String filename) {
        BufferedReader BReader = null ;
        try {
            BReader = new BufferedReader(new FileReader(new File(filename)));
        } catch (FileNotFoundException Ex){
            LOG.error("File not found (openFileBR): " + filename);
            System.exit(-1);
        }
        return BReader;
    }

    public static class SimJoinPrefFilterMapper extends Mapper<Text, Text, Text, Text> {
        private Text PrefixWords = new Text();
        private float threshold = 0.8f;

        @Override
        public void map(Text docId, Text wordList, Context context) throws IOException, InterruptedException {

            String[] wordsL = wordList.toString().split(" ");
            // We only take the first |doc| - [threshold. |doc|] + 1 of the PrefixWords list
            int prefixFilteringIndex = Math.round(wordsL.length - (wordsL.length * threshold) + 1);
            String[] PrefixFiltering = Arrays.copyOfRange(wordsL, 0, prefixFilteringIndex);
            // we emit an inverted index
            for (String WordsInPrefix : PrefixFiltering) {
                PrefixWords.set(WordsInPrefix);
                context.write(PrefixWords, docId);
            }
        }
    }

    public static class SimJoinPrefFilterReducer extends Reducer<Text, Text, Text, Text> {

        private BufferedReader reader = openFileBR("/home/yaj/devel/DSBA/mdp/Assign2/results/InputPreProcess.res");

        @Override
        public void reduce(Text prefixWords, Iterable<Text> docId, Context context)
                throws IOException, InterruptedException
        {
            HashMap<String, String> docLinesHM = new HashMap<>();

            // Build a HashMap docLinesHM containing = (docId, words list)
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                String[] line = currentLine.split(",");
                docLinesHM.put(line[0], line[1]);
            }

            ArrayList<Text> docIdList = new ArrayList<>();
            for (Text id: docId) {
                docIdList.add(id);
            }

            int numPrefixMatchingDocs = docIdList.size();

            if (numPrefixMatchingDocs > 1) {
                // Create all possible pairs of matching documents
                TreeSet<DocPair> matchingDocPairsTS = new TreeSet<>();
                for (int i = 0; i < numPrefixMatchingDocs; ++i) {
                    for (int j = i + 1; j < numPrefixMatchingDocs; ++j) {
                        DocPair matchingPairs = new DocPair(docIdList.get(i), docIdList.get(j));
                        matchingDocPairsTS.add(matchingPairs);
                    }
                }

                DocPair dp = matchingDocPairsTS.pollFirst();
                // For each document pairs
                while (dp != null) {
                    //get the first and second document name of the pair
                    String firstDoc = dp.getDoc1().toString();
                    String SecondDoc = dp.getDoc2().toString();

                    // fetch the text from the HashMap docLinesHM
                    String[] firstDocWordStr = docLinesHM.get(firstDoc).split(" ");
                    String[] secondDocWordStr = docLinesHM.get(SecondDoc).split(" ");

                    // build an ordered TreeSet of unique elements
                    TreeSet<String> FirstDocWordsTS = new TreeSet<>();
                    TreeSet<String> secondDocWordsTS = new TreeSet<>();
                    Collections.addAll(FirstDocWordsTS, firstDocWordStr);
                    Collections.addAll(secondDocWordsTS, secondDocWordStr);

                    // compute the Jaccard Similarity and update the number of comparison
                    double jaccardSimilarity = new JaccardSimilarity().compute(FirstDocWordsTS, secondDocWordsTS);
                    context.getCounter(EnumCounter.PREFIX_FILTERING_CMP).increment(1);

                    if (jaccardSimilarity >= 0.8) {
                        context.write(new Text("(" + firstDoc + ", "+ SecondDoc + ")"),
                                new Text(String.valueOf(jaccardSimilarity)));
                    }

                    // get next pair
                    dp = matchingDocPairsTS.pollFirst();
                }
            }
        }
    }
}