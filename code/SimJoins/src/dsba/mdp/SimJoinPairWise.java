package dsba.mdp;

import java.io.*;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * Created by @yaj
 *
 * @date: 06/06/16.
 */


public class SimJoinPairWise extends Configured implements Tool {

    public enum EnumCounter {PAIR_WISE_COMP}
    private static final Logger LOG = Logger.getLogger(SimJoinPairWise.class);

    private static void usage() {
        System.err.println("Usage: SimJoinPairWise <in> <out>");
        ToolRunner.printGenericCommandUsage(System.err);
        System.exit(2);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(Arrays.toString(args));
        int res = ToolRunner.run(new Configuration(),new SimJoinPairWise(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        System.out.println(Arrays.toString(args));
        Job simJoinPWJob = Job.getInstance(getConf(), "SimJoinPairWise");

        if (args.length !=2) {
            LOG.error(Arrays.toString(args));
            usage();
        }

        simJoinPWJob.setJarByClass(SimJoinPairWise.class);
        simJoinPWJob.setMapperClass(SimJoinPairWiseMapper.class);
        simJoinPWJob.setReducerClass(SimJoinPairWiseReducer.class);
        simJoinPWJob.setMapOutputKeyClass(DocPair.class);
        simJoinPWJob.setMapOutputValueClass(Text.class);
        simJoinPWJob.setOutputKeyClass(Text.class);
        simJoinPWJob.setOutputValueClass(Text.class);
        simJoinPWJob.setInputFormatClass(KeyValueTextInputFormat.class);
        simJoinPWJob.setOutputFormatClass(TextOutputFormat.class);
        simJoinPWJob.getConfiguration().set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", ",");
        simJoinPWJob.getConfiguration().set("mapreduce.output.textoutputformat.separator", ", ");

        FileSystem fs = FileSystem.newInstance(getConf());


        Path inFile = new Path(args[0]);
        Path outFile = new Path("SimJoinPairWise" + "/" + args[1]);


        if (fs.exists(outFile)) {
            fs.delete(outFile, true);
            LOG.info("Deleted " + outFile);
        }

        FileInputFormat.addInputPath(simJoinPWJob, inFile);
        FileOutputFormat.setOutputPath(simJoinPWJob, outFile);

        int ret = (simJoinPWJob.waitForCompletion(true)?0:1);
        createCounterFile(simJoinPWJob, fs, new Path("SimJoinPairWise/PAIR_WISE_COMP.txt"));
        return ret;
    }

    private void createCounterFile(Job job, FileSystem fs, Path filename) throws IOException {
        if (fs.exists(filename)) {
            fs.delete(filename, true);
            LOG.info("Deleted " + filename);
        }
        long compCount = job.getCounters().findCounter(EnumCounter.PAIR_WISE_COMP).getValue();
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(fs.create(filename, true)));
        br.write(String.valueOf(compCount));
        br.close();
    }

    private static BufferedReader openFileBR(String filename) {
        BufferedReader BReader = null ;
        try {
            BReader = new BufferedReader(new FileReader(new File(filename)));
        } catch (FileNotFoundException Ex){
            LOG.error("File not found (openFileBR): " + filename);
            System.exit(-1);
        }
        return BReader;
    }

    public static class SimJoinPairWiseMapper extends Mapper<Text, Text, DocPair, Text> {

        // we get the input from the local directory
        private BufferedReader reader = openFileBR("/home/yaj/devel/DSBA/mdp/Assign2/results/InputPreProcess.res");
        private static DocPair docPair = new DocPair();

        @Override
        public void map(Text key, Text value, Context context)
                throws IOException, InterruptedException
        {
            HashMap<String, String> lineHM = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split(",");
                lineHM.put(words[0], words[1]);
            }

            for (String lhm : lineHM.keySet()) {
                if (!key.toString().equals(lhm)) {
                    docPair.set(key, new Text(lhm));
                    context.write(docPair, new Text(value.toString()));
                }
            }
        }
    }

    public static class SimJoinPairWiseReducer extends Reducer<DocPair, Text, Text, Text> {

        private BufferedReader reader = openFileBR("/home/yaj/devel/DSBA/mdp/Assign2/results/InputPreProcess.res");


        @Override
        public void reduce(DocPair key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            HashMap<String, String> firstDocLinesHM = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split(",");
                //linesHM = (doc1, words1)
                firstDocLinesHM.put(words[0], words[1]);
            }

            String[] firstDocWordStr = values.toString().split(" ");
            String[] secondDocWordStr = firstDocLinesHM.get(key.getDoc2().toString()).split(" ");

            TreeSet<String> FirstDocWordsTS = new TreeSet<>();
            TreeSet<String> secondDocWordsTS = new TreeSet<>();

            Collections.addAll(FirstDocWordsTS, firstDocWordStr);
            Collections.addAll(secondDocWordsTS, secondDocWordStr);

            context.getCounter(EnumCounter.PAIR_WISE_COMP).increment(1);
            double jaccardSimilarity = new JaccardSimilarity().compute(FirstDocWordsTS, secondDocWordsTS);

            if (jaccardSimilarity >= 0.8) {
                context.write(new Text("(" + key.getDoc1() + ", " + key.getDoc2() + ")"),
                        new Text(String.valueOf(jaccardSimilarity)));
            }
        }
    }
}
